## StudyNoteJS 
#####(made for Swamphacks 2016)

The StudyNote.JS web application is designed for those with a lot to read, and not enough time to read it. The algorithms in our solution provide an easy way to convert large chunks of boring and long text into quick and easy to read bullet points.

Through careful processing of each word, and the use of machine learning via the DatumBox API, we are able to select just the right balance of content and brevity to deliver a highly portable PDF document right in your web browser. The DatumBox API is accessed through a Python-dependent backend, which is able to be leveraged by Amazon AWS cloud severs. A variety of formatting options are available to make your study experience comfortable for any eyes.

On those long nights of research, an easy-to-use extension to the Google Chrome web browser allows users to condense pieces of articles into slick statements that are easy to discern and categorize with a quick glance. The aforementioned API from DatumBox helps to lift that burden too by assessing the complexity of the processed text and assigning one of 16 topics to the bulleted content.

Check out  [our publication on Devpost](http://devpost.com/software/studynote-js).
